HTML5Tidy
=========

Simple wrapper around html5lib to "tidy" html in the wild to
well-formed xml/html


Installation
------------

python setup.py install

Usage
-----

With the command-line:

usage:  [-h] [--fragment] [--indent] [--mogrify] [--method METHOD]
        [--output OUTPUT] [--title TITLE] [--charset CHARSET]
        [--script SCRIPT] [--stylesheet STYLESHEET]
        [--alternate ALTERNATE [ALTERNATE ...]] [--next NEXT [NEXT ...]]
        [--prev PREV [PREV ...]] [--search SEARCH [SEARCH ...]]
        [--rss RSS [RSS ...]] [--atom ATOM [ATOM ...]]
        [input]

positional arguments:
  input

optional arguments:
  -h, --help            show this help message and exit
  --fragment
  --indent
  --mogrify             modify file in place
  --method METHOD       method, default: html, values: html, xml, text
  --output OUTPUT
  --title TITLE         ensure/add title tag in head
  --charset CHARSET     ensure/add meta tag with charset
  --script SCRIPT       ensure/add script tag
  --stylesheet STYLESHEET
                        ensure/add style link
  --alternate ALTERNATE [ALTERNATE ...]
                        ensure/add alternate links (optionally followed by a
                        title and type)
  --next NEXT [NEXT ...]
                        ensure/add alternate link
  --prev PREV [PREV ...]
                        ensure/add alternate link
  --search SEARCH [SEARCH ...]
                        ensure/add search link
  --rss RSS [RSS ...]   ensure/add alternate link of type application/rss+xml
  --atom ATOM [ATOM ...]
                        ensure/add alternate link of type application/atom+xml

With the python module

    >>> from html5tidy import tidy
    >>> tidy('some text')
    '<html><head><meta charset="utf-8"></head><body>some text</body></html>'


Changelog
-----------

* 2017: Removing lxml dependency. Based now on html5lib + built in elementtree. Added page modifier options (like adding/ensuring a particular stylesheet or script tag)

To do
-------------
* Merge added command-line functionality back to python functions (like ensuring/adding link elements)

Copyright
---------

- 2011, 2012 [The active archives contributors](http://activearchives.org/)
- 2011, 2012 Michael Murtaugh
- 2017 Michael Murtaugh

All rights reserved.

This software is released under the GPL3 license. See gpl-3.0.txt for details.
